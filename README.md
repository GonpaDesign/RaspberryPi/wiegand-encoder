WIEGAND ENCODER
=======

## Descripción

Programa para enviar pulsos desde la Raspberry Pi 3 Model B+ a un equipo biometrico por protocolo Wiegand. 

El programa necesita recibir dos parametros, con los cuales realiza la configuracion. El primer parametro sera la cantidad de pulsos (26 o 34), el segundo sera el numero a convertir al formato Wiegand (calculando paridades).

## Programación, librerias y compilación

El programa se encuentra escrito en C++, y compilado desde el Geany de la Raspberry Pi.

Se requiere tener instalada las librerias:

1. wiringPi (Viene preinstalada en el Raspbian)
2. jsoncpp

Para poder compilarlo hay que configurar el builder del geany y que el BUILD figure de la siguietne forma:

g++ -Wall -o "%e" "%f" -lwiringPi -ljsoncpp

## Instalación

Para instalar el software debemos tener instalado los paquetes:

* libjsoncpp1
* libjsoncpp-dev

La ruta de instalacion (Se debe crear si no existe), es:

/usr/programs/wiegandEncode/

Los logs, se guardarna en la siguiente ubicacion (Se debe crear si no existe):

/var/log/wiegandEncode/event.log

## Configuración

Se cuenta con un unico archivo de configuracion. A continuacion se los enumera y detalla su uso.

*system.json

    Aqui se configuran los pines a utilizar para la lectura de los WD0 y WD1, ademas que se puede activar o desactivar el modo 
    DEBUG para obtener como salida todos los procesos en ejecucion durante el uso del codigo. Tambien se puede configurar el 
    tiempo del pulso y el intervalo para el formato Wiegand.