#include <wiringPi.h> 				/* access to GPIO of Raspberry Pi */
#include <stdio.h> 				/* printf, sprintf */
#include <time.h>
#include <string.h>				/* memcpy, memset */
#include <string>
#include <jsoncpp/json/json.h> 			/* Json parser*/ // or jsoncpp/json.h , or json/json.h etc.
#include <iostream>
#include <fstream>

#define LOG_FILE "/var/log/wiegandEncode/event.log"
#define CONFIG_FILE "/usr/programs/wiegandEncode/system.json"

int WD0 = 4;					// Negro o Verde
int WD1 = 5;					// Rojo o Blanco
int debug = 1;
int format = 34;
int pulse = 50;
int interval = 200;

using namespace std;

std::string code = "";

void log(int type, std::string description)
{
	time_t rawTime;
	struct tm * timeinfo;
	
	time(&rawTime);
	timeinfo = localtime (&rawTime);
	
	FILE *f = fopen(LOG_FILE, "a");
	
	if(f == NULL)
	{
		printf("Error opening file");
	}
	
	char Date[20];
	sprintf(Date, "%02d/%02d/%d  %02d:%02d:%02d", timeinfo->tm_mday, timeinfo->tm_mon + 1,timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
	
	switch(type)
	{
		case 0:
			fprintf(f, "%s	%s	%s\n", Date, "DEBUG", description.c_str());
		break;
		case 1:
			fprintf(f, "%s	%s	%s\n", Date, "INFO", description.c_str());
		break;
		case 2:
			fprintf(f, "%s	%s	%s\n", Date, "ERROR", description.c_str());
		break;
	}
	
	fclose(f);
}

std::string parityCalc(std::string original){
  std::string wg = original;
  int pCount = 0;
  int midPoint = format / 2;
  int endBit = format - 1;
  for(int index = 0; index < midPoint; index++)
  {
    if(wg[index] != '0')
    {
      pCount++;
    }
  }

  if((pCount % 2) != 0)
  {
    wg[0] = '1';
  }

  pCount = 0;
  for(int index = midPoint; index < format; index++)
  {
    if(wg[index] != '0')
    {
      pCount++;
    }
  }

  if((pCount % 2) == 0)
  {
    wg[endBit] = '1';
  }

  return wg;
}

void convert(long dni)
{
  int endBit = format - 1;
  std::string temp = "";
  for(int index = 0; index < format; index++)
  {
    if(index < 2)
    {
      temp += "0";
    }
    else
    {
      int mask = 1;
      mask <<= (endBit - (index + 1));
      mask &= dni;
      if(mask == 0)
      {
        temp += "0";
      }
      else
      {
        temp += "1";
      }
    }
  }

  code = parityCalc(temp);
}

void writeD0()
{
	digitalWrite(WD0, LOW);
	delayMicroseconds(pulse);
	digitalWrite(WD0, HIGH);
}

void writeD1()
{
	digitalWrite(WD1, LOW);
	delayMicroseconds(pulse);
	digitalWrite(WD1, HIGH);
}

void sendData(){
  /* Inicio - Modificacion para despertar el RELOJ */
  /*
  digitalWrite(WD1, LOW);
  digitalWrite(WD1, LOW);
  delay(2);
  digitalWrite(WD1, HIGH);
  digitalWrite(WD1, HIGH);
  delay(20);
  * */
  /* Fin - Modificacion para despertar el RELOJ */
  for(int index = 0; index < format; index++)
  {
    code[index]=='0' ? writeD0() : writeD1();
    delayMicroseconds(interval);
  }
}

int readSystemConfiguration()
{
	try
	{
		ifstream systemConfig(CONFIG_FILE);
		Json::Reader reader;
		Json::Value jsonObj;
		reader.parse(systemConfig, jsonObj); 
		const Json::Value& characters = jsonObj["system"]; // array of characters
		WD0 = characters["WD0pin"].asInt();
		WD1 = characters["WD1pin"].asInt();
		pulse = characters["pulse"].asInt();
		interval = characters["interval"].asInt();
		debug = characters["debug"].asInt();

		if(debug == 1)
		{
		  cout << endl;
		  cout << "---- Configuration ----" << endl;
		  cout << "WD0 pin: " << WD0 << endl;
		  cout << "WD1 pin: " << WD1 << endl;
		  cout << "Pulse time: " << pulse << " microsec." << endl;
		  cout << "Interval time: " << interval / 1000 << " millisec." << endl;
		  cout << endl;
		}
		
		return 1;
	}catch(exception& e){
		return 0;
	}
}

int main(int argc, char *argv[])
{
	if(wiringPiSetup() < 0){return 0;}
	
	if(readSystemConfiguration() < 0){return 0;}

	format = atol(argv[1]);
	
	pinMode(WD0, OUTPUT);
	pinMode(WD1, OUTPUT);
	digitalWrite(WD0, HIGH);
	digitalWrite(WD1, HIGH);
	
	std::string logInfo = "Send number: ";
	logInfo += argv[2];
	
	log(1, logInfo);

	convert(atol(argv[2]));
	sendData();
	
	log(1, code);
}
